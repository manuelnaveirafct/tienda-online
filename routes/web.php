<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::bind('product',function($id){
    return App\Models\Product::where('id',$id)->first();
});

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout');

/*
|--------------------------------------------------------------------------
| Config Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => ['auth']], function () {

    Route::get('order-detail', 'CartController@orderDetail')->name('order-detail');

    Route::prefix('config')->group(function () {

        Route::get('/', 'PanelController@index')->name('config.user.index');

        /**
         * Categories
         */
        Route::get('/categories', 'Config\Category\CategoryController@index')->name('config.category.index');
        Route::get('/category/create', 'Config\Category\CategoryController@create')->name('config.category.create');
        Route::post('/category/store', 'Config\Category\CategoryController@store')->name('config.category.store');
        Route::get('/category/{category}/show', 'Config\Category\CategoryController@show')->name('config.category.show');
        Route::get('/category/{category}/edit', 'Config\Category\CategoryController@edit')->name('config.category.edit');
        Route::put('/category/{category}/update', 'Config\Category\CategoryController@update')->name('config.category.update');
        Route::delete('/category/{category}/delete', 'Config\Category\CategoryController@delete')->name('config.category.delete');

        /**
         * Products
         */
        Route::get('/products', 'Config\Product\ProductController@index')->name('config.product.index');
        Route::get('/product/create', 'Config\Product\ProductController@create')->name('config.product.create');
        Route::post('/product/store', 'Config\Product\ProductController@store')->name('config.product.store');
        Route::get('/product/{product}/show', 'Config\Product\ProductController@show')->name('config.product.show');
        Route::get('/product/{product}/edit', 'Config\Product\ProductController@edit')->name('config.product.edit');
        Route::put('/product/{product}/update', 'Config\Product\ProductController@update')->name('config.product.update');
        Route::delete('/product/{product}/delete', 'Config\Product\ProductController@delete')->name('config.product.delete');
        
        
        Route::delete('/product/{product}/images/{image}/delete', 'Config\Product\ImageController@delete')->name('config.product.image.delete');

        /**
         * Users
         */
        Route::get('/users', 'Config\User\UserController@index')->name('config.user.index');
        Route::get('/user/create', 'Config\User\UserController@create')->name('config.user.create');
        Route::post('/user/store', 'Config\User\UserController@store')->name('config.user.store');
        Route::get('/user/{user}/show', 'Config\User\UserController@show')->name('config.user.show');
        Route::get('/user/{user}/edit', 'Config\User\UserController@edit')->name('config.user.edit');
        Route::put('/user/{user}/update', 'Config\User\UserController@update')->name('config.user.update');
        Route::delete('/user/{user}/delete', 'Config\User\UserController@delete')->name('config.user.delete');

        /**
         * Order
         */
        Route::get('/orders', 'Config\Order\OrderController@index')->name('config.order.index');
        Route::get('/order/create', 'Config\Order\OrderController@create')->name('config.order.create');
        Route::post('/order/store', 'Config\Order\OrderController@store')->name('config.order.store');
        Route::get('/order/{order}/show', 'Config\Order\OrderController@show')->name('config.order.show');
        Route::get('/order/{order}/edit', 'Config\Order\OrderController@edit')->name('config.order.edit');
        Route::put('/order/{order}/update', 'Config\Order\OrderController@update')->name('config.order.update');
        Route::delete('/order/{order}/delete', 'Config\Order\OrderController@delete')->name('config.order.delete');
    });    

});



Route::get('/', 'HomeController@login')->name('logueado');

Route::get('/', 'HomeController@index')->name('home');

Route::get('/pago', 'HomeController@pago')->name('pago');

Route::get('/envio', 'HomeController@envio')->name('envio');

Route::get('/politics', 'HomeController@politics')->name('politics');

Route::get('/conditions', 'HomeController@conditions')->name('conditions');

Route::get('/products/category/{category}', 'Config\Product\ProductController@buscarPorCategoria')->name('config.product.buscarPorCategoria');

Route::get('/config/product/{product}/images/{image}/show', 'Config\Product\ImageController@show')->name('config.product.image.show');

Route::get('/product/{productId}', 'Config\Product\ProductController@findBy')->name('config.product.findBy');

Route::get('cart/show', 'CartController@show' )->name('cart-show');

Route::get('cart/add/{product}', 'CartController@add' )->name('cart-add');

Route::get('cart/delete/{product}', 'CartController@delete' )->name('cart-delete');
Route::get('cart/trash', 'CartController@trash' )->name('cart-trash');
Route::get('cart/update/{product}/{quantity?}', 'CartController@update' )->name('cart-update');

// Detalle del producto


//paypal
Route::get('payment', 'PaypalController@postPayment')->name('payment');
Route::get('payment/status', 'PaypalController@getPaymentStatus')->name('payment.status');
