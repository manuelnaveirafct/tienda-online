<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(

            //PATUCOS
            [
               'category_id' => 1,
               'name' =>"Patucos beige",
               'ruta' => "/config/product/1/images/1/show",
               'description' => "Patucos color beige",
               'price' => 16,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            [
                'category_id' => 1,
                'name' =>"Patucos Rosa",
                'ruta' => "/config/product/2/images/2/show",
                'description' => "Patucos color Rosa",
                'price' => 16,
                'stock' => 1,
                'discount' => NULL,
                'created_at' => new DateTime,
                'updated_at' =>new DateTime
            ],

            [
                'category_id' => 1,
               'name' =>"Patucos pompon",
               'ruta' => "/config/product/3/images/3/show",
               'description' => "Patucos con pompon",
               'price' => 16,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime

            ],

            [
               'category_id' => 1,
               'name' =>"Patucos 2 colores",
               'ruta' => "/config/product/4/images/4/show",
               'description' => "Patucos en color azul y blanco",
               'price' => 16,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            [
               'category_id' => 1,
               'name' =>"Patucos azul",
               'ruta' => "/config/product/5/images/5/show",
               'description' => "Patucos en color azul claro",
               'price' => 16,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime

            ],

            [
               'category_id' => 1,
               'name' =>"Patucos gris",
               'ruta' => "/config/product/6/images/6/show",
               'description' => "Patucos en color gris",
               'price' => 16,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            // BUFANDAS

            [
               'category_id' => 2,
               'name' =>"Bufanda colorida",
               'ruta' => "/config/product/7/images/7/show",
               'description' => "Bufanda bufanda con varios colores",
               'price' => 20,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            [
                'category_id' => 2,
                'name' =>"Bufanda Azul",
                'ruta' => "/config/product/8/images/8/show",
                'description' => "Bufanda color azul",
                'price' => 20,
                'stock' => 1,
                'discount' => NULL,
                'created_at' => new DateTime,
                'updated_at' =>new DateTime
            ],

            [
                'category_id' => 2,
               'name' =>"Bufanda cuadros",
               'ruta' => "/config/product/9/images/9/show",
               'description' => "Bufanda con cuadros de colores",
               'price' => 20,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime

            ],

            [
               'category_id' => 2,
               'name' =>"Bufanda verde",
               'ruta' => "/config/product/10/images/11/show",
               'description' => "Bufanda con color verde",
               'price' => 20,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            [
               'category_id' => 2,
               'name' =>"Bufanda negra",
               'ruta' => "/config/product/11/images/11/show",
               'description' => "Bufanda color negro",
               'price' => 20,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime

            ],

            [
               'category_id' => 2,
               'name' =>"Bufanda cuadros oscuro",
               'ruta' => "/config/product/12/images/12/show",
               'description' => "Bufanda con cuadros azules y negros",
               'price' => 20,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            // CONJUNTOS

            [
               'category_id' => 3,
               'name' =>"Conjunto amarillo",
               'ruta' => "/config/product/13/images/13/show",
               'description' => "Conjunto en color amarillo",
               'price' => 50,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            [
                'category_id' => 3,
                'name' =>"Conjunto azul claro",
                'ruta' => "/config/product/14/images/14/show",
                'description' => "Conjunto color azul claro",
                'price' => 50,
                'stock' => 1,
                'discount' => NULL,
                'created_at' => new DateTime,
                'updated_at' =>new DateTime
            ],

            [
               'category_id' => 3,
               'name' =>"Conjunto azul",
               'ruta' => "/config/product/15/images/15/show",
               'description' => "Conjunto en color azul oscuro",
               'price' => 50,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime

            ],

            [
               'category_id' => 3,
               'name' =>"Conjunto claro",
               'ruta' => "/config/product/16/images/16/show",
               'description' => "Conjunto en colores claros",
               'price' => 50,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            [
               'category_id' => 3,
               'name' =>"Conjunto rosa",
               'ruta' => "/config/product/17/images/17/show",
               'description' => "Conjuntos en color rosa",
               'price' => 50,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime

            ],

            [
               'category_id' => 3,
               'name' =>"Conjunto rosa claro",
               'ruta' => "/config/product/18/images/18/show",
               'description' => "Conjuntos en color rosa claro",
               'price' => 50,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            //GORROS

            [
               'category_id' => 4,
               'name' =>"Gorro negro",
               'ruta' => "/config/product/19/images/19/show",
               'description' => "Gorro en color negro",
               'price' => 22,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            [
                'category_id' => 4,
                'name' =>"Gorro amarillo",
                'ruta' => "/config/product/20/images/20/show",
                'description' => "Gorro en color amarillo",
                'price' => 22,
                'stock' => 1,
                'discount' => NULL,
                'created_at' => new DateTime,
                'updated_at' =>new DateTime
            ],

            [
                'category_id' => 4,
               'name' =>"Gorro marrón",
               'ruta' => "/config/product/21/images/21/show",
               'description' => "Gorros en color marrón",
               'price' => 22,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime

            ],

            [
               'category_id' => 4,
               'name' =>"Gorro blanco",
               'ruta' => "/config/product/22/images/22/show",
               'description' => "Gorro blanco con lazo",
               'price' => 22,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            [
               'category_id' => 4,
               'name' =>"Gorro rosa",
               'ruta' => "/config/product/23/images/23/show",
               'description' => "Gorros en color rosa",
               'price' => 22,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime

            ],

            [
               'category_id' => 4,
               'name' =>"Gorro con lazo",
               'ruta' => "/config/product/24/images/24/show",
               'description' => "Gorros con lazo ",
               'price' => 22,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            //RANITAS

            [
               'category_id' => 5,
               'name' =>"Ranita blanca",
               'ruta' => "/config/product/25/images/25/show",
               'description' => "Ranita blanca con dibujos",
               'price' => 25,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            [
                'category_id' => 5,
                'name' =>"Ranitas amarilla",
                'ruta' => "/config/product/26/images/26/show",
                'description' => "Ranitas en color amarillo",
                'price' => 20,
                'stock' => 1,
                'discount' => NULL,
                'created_at' => new DateTime,
                'updated_at' =>new DateTime
            ],

            [
               'category_id' => 5,
               'name' =>"Ranita roja",
               'ruta' => "/config/product/27/images/27/show",
               'description' => "Ranita en color rojo",
               'price' => 25,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime

            ],

            [
               'category_id' => 5,
               'name' =>"Ranita gris",
               'ruta' => "/config/product/28/images/28/show",
               'description' => "Ranitas en color gris",
               'price' => 20,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ],

            [
               'category_id' => 5,
               'name' =>"Ranita negra",
               'ruta' => "/config/product/29/images/29/show",
               'description' => "Ranitas en color negro",
               'price' => 25,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime

            ],

            [
               'category_id' => 5,
               'name' =>"Ranita azul",
               'ruta' => "/config/product/30/images/30/show",
               'description' => "Ranita en color azul",
               'price' => 20,
               'stock' => 1,
               'discount' => NULL,
               'created_at' => new DateTime,
               'updated_at' =>new DateTime
            ]

            );
    Product::insert($data);
}
}


