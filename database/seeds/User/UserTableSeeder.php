<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data=array(

            [
                
                'name' => "manuel",
                'surname' => "naveira",
                'address' => "Rua das mariñas 23 2D",
                'localidad' => "Betanzos",
                'provincia' => "A Coruña",
                'email' => "manuel@naveira.com",
                'email_verified_at' => NULL,
                'password' => bcrypt('naveira7'),
                'is_admin' => true,
                'remember_token' => NULL,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],

            [
                
                'name' => "user",
                'surname' => "username",
                'address' => "user 22",
                'localidad' => "Betanzos",
                'provincia' => "A Coruña",
                'email' => "user@user.com",
                'email_verified_at' => NULL,
                'password' => bcrypt('naveira7'),
                'is_admin' => false,
                'remember_token' => NULL,
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ]
        );
        User::insert($data);
    }
}
