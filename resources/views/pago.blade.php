<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Reenie+Beanie&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/0763b1b783.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/stylepago.css') }}">
    <title>Métodos de pago</title>
</head>
<body>
    <header class="d-block ">
        <div class="row">
            <div class="col px-0">
               <a href="{{ route('home') }}" class="text-decoration-none text-dark"><h1 class="py-4 text-center ">Muñecos De Trapo Shop</h1></a> 
            </div>
        </div>
    </header>

    <!--  -->
    <div class="container">
            <div class="col text-left">
                    <a href="#" class="text-decoration-none"><i class="far fa-money-bill-alt 2-px"></i></i><span> Métodos de pago</span></a>
                </div> 
            <ul class="list-group list-group-flush pt-4">
                <li class="list-group-item"><b>Existen ciertos métodos que puedes utilizar en Muñecos De Trapo Shop:</b></li>
                <li class="list-group-item">Pagos en efectivo</li>
                <li class="list-group-item">Transferencias bancarias</li>
                <li class="list-group-item">PayPal</li>
            </ul>
    </div>
</body>
</html>