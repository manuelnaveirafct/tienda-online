@extends('confpaneltitle')

@section('container')
<div class="row">
    <div class="col-md-6">
        <div class="panel text-center py-4">
            <i class="fab fa-cuttlefish fa-2x"></i>
            <a href="{{ route('config.category.index') }}" class="btn btn-dark btn-block">CATEGORÍAS</a>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel text-center py-4">
        <i class="fab fa-product-hunt fa-2x"></i>
            <a href="{{ route('config.product.index') }}" class="btn btn-dark btn-block">PRODUCTOS</a>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel text-center py-4">
            <i class="fas fa-user fa-2x"></i>
            <a href="{{ route('config.user.index') }}" class="btn btn-dark btn-block">USUARIOS</a>
        </div>
    </div>
    <div class="col-md-6">
            <div class="panel text-center py-4">
                <i class="far fa-credit-card fa-2x"></i>
                <a href="{{ route('config.order.index') }}" class="btn btn-dark btn-block">PEDIDOS</a>
            </div>
    </div>
</div>
<link type="text/css" rel="stylesheet" href="{{ mix('css/app.css') }}">
@endsection