<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Reenie+Beanie&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/0763b1b783.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/styleorder.css') }}">
    <title>order detail</title>
</head>
<body>
        <header class="d-block">
                <div class="row">
                    <div class="col px-0">
                       <a href="{{ route('home') }}" class="text-decoration-none text-dark"><h1 class="py-4 text-center">Muñecos De Trapo Shop</h1></a> 
                    </div>
                </div>
            </header>

    <!--  -->
    <!-- <div class="container">
        
    </div> -->

    <div class="container pb-4 text-center bg-light">
        <div class="page-header text-left">
            <h2>Detalle del producto</h2>
        </div>

        <div class="page">
            <div class="table-responsive">
                <h3>Datos del usuario</h3>
                <table class="table table-striped table-hover table-bordered">
                    <tr><td>Nombre:</td><td>{{ Auth::user()->name }}</td></tr>
                    <tr><td>Apellido:</td><td>{{ Auth::user()->surname }}</td></tr>
                    <tr><td>Correo:</td><td>{{ Auth::user()->email }}</td></tr>
                    <tr><td>Direccion:</td><td>{{ Auth::user()->address }}</td></tr>
                    <tr><td>Localidad:</td><td>{{ Auth::user()->localidad }}</td></tr>
                    <tr><td>Provincia:</td><td>{{ Auth::user()->provincia }}</td></tr>

                </table>
            </div>

            <div class="table-responsive">
                <h3>Datos del pedido</h3>
                <table class="table table-striped table-hover table-bordered">
                    <tr>
                        <td>Producto</td>
                        <td>Precio</td>
                        <td>Cantidad</td>
                        <td>Subtotal</td>
                    </tr>
                    @foreach($cart as $product)
                    <tr>
                        <td>{{ $product->name }}</td>
                        <td>{{ number_format($product->price,2) }}€</td>
                        <td>{{ $product->quantity }}</td>
                        <td>{{ number_format($product->price * $product->quantity, 2) }}€</td>


                    </tr>
                    @endforeach
                </table>
                <h3>
                    <span class="label label-sucess">
                        Total: {{ number_format($total,2)}}€
                    </span>    
                </h3><hr>

                <p>
                    <a href="{{ route('cart-show') }}" class="btn btn-secondary">
                        <i class="fa fa-chevron-circle-left"></i>Regresar
                    </a>

                    <a href="{{ route('payment') }}" class="btn btn-secondary">
                        Pagar con <i class="fab fa-paypal"></i>
                    </a>
                </p>
            </div>
        </div>    
    </div>
     

</body>
</html>