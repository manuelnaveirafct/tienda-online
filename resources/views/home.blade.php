<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Reenie+Beanie&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/0763b1b783.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <title>MuñecosDeTrapoShop</title>
</head>
<body class="overflow-x-hidden">
        @if(\Session::has('message'))
		@include('message')
	    @endif
    <header class="d-block" id="app">
        <div class="row">
            <div class="col px-0">
                <h1 class="py-4 text-center">Muñecos De Trapo Shop</h1>
            </div>
        </div>

        <div class="row">
            <div class="col text-center">
                @if (!Auth::check())
                    <a href="{{ route('login') }}"><i class="fas fa-user-friends fa-2x" aria-hidden="true"></i></a>
                @else
                    <a href="{{ route('logout') }}">Desconectarse</a>
                @endif
            </div>
            <searcher></searcher>
            <div class="col carrito text-center">
                <a href="{{ route('cart-show') }}"><i class="fas fa-shopping-cart fa-2x" aria-hidden="true"></i></a> 
            </div>
        </div>
    </header>

    <div class="container">
        <section class="bg-light full-width py-4">
            <div class="container py-4">
                <div class="row">
                    <div class="col">
                        <div class="accordion" id="accordionExample">
                            <div class="card products">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link text-decoration-none" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            Productos
                                        </button>
                                    </h2>
                                    </div>
                                
                                    <div id="collapseOne" class="btn text-decoration-none btn-link collapsed p-0" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body p-0 text-decoration-none">
                                            <ul class="list-unstyled text-left aside-home mb-0 ">
                                            @foreach($categories as $category)
                                                <a class="text-decoration-none" href="{{ route('config.product.buscarPorCategoria', $category->id ) }}">
                                                    <li class="p-3 ">{{$category->name}}</li>
                                                </a>
                                            @endforeach
                                            </ul>
                                        </div>
                                    </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed text-decoration-none" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Redes Sociales
                                    </button>
                                </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <a href="#">Actualmente contamos con página de Instagram. (munecosdetraposhop)</a> 
                                </div>
                                </div>
                            </div>
                            <!--  -->
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                  <h2 class="mb-0">
                                    <button class="btn btn-link collapsed text-decoration-none"  type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                      Contactanos
                                    </button>
                                  </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                  <div class="card-body">
                                        <a href="#"><p><i class="fas fa-mobile"><span> Teléfono: 666-666-666 </span></i></p></a>
                                        <a href="#"><p><i class="fas fa-envelope"> <span> Mail: munecodetraposhop@gmail.com </span> </i></p></a>
                                  </div>
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="col slider">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item">
                                        <img src="{{ asset('images/1.jpg') }}" class="d-block w-100" alt="...">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="{{ asset('images/2.jpg') }}" class="d-block w-100" alt="...">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="{{ asset('images/3.jpg') }}" class="d-block w-100" alt="...">
                                    </div>
                                    <div class="carousel-item active">
                                        <img src="{{ asset('images/4.jpg') }}" class="d-block w-100" alt="...">
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <footer class="bg-dark full-width pt-4">
            <div class="container py-4">
                <div class="py-4 row text-white">
                    <div class="col">
                        <h4>EMPRESA</h4>
                        <a class="d-block text-decoration-none" href="{{ route('conditions') }}" ><i class="fas fa-shopping-bag "></i> <span>CONDICIONES DE USO Y COMPRA</span></a> 
                    </div>
    
                    <div class="col">
                        <h4>SÍGUENOS</h4>
                        <a class="d-block text-decoration-none" href="https://www.instagram.com/munecosdetraposhop/?hl=es"><i class="fab fa-instagram"></i><span> INSTAGRAM</span></a>
                    </div>
    
                    <div class="col">
                        <h4>AYUDA</h4>
                        <a class="d-block text-decoration-none" href="{{ route('pago') }}"><i class="fab fa-cc-amazon-pay"></i><span> PAGO</span></a>
                        <a class="d-block text-decoration-none" href="{{ route('envio') }}"><i class="far fa-paper-plane"></i><span> ENVÍO</span></a>
                        <a class="d-block text-decoration-none" href="{{ route('politics') }}"><i class="fab fa-product-hunt"></i><span> POLÍTICA</span></a>  
                    </div>                   
                </div>
            </div>
        </footer>

        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        
        <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>