<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Reenie+Beanie&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/0763b1b783.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/stylecarrito.css') }}">
    <title>Carrito</title>
</head>
<body>
        <header class="d-block">
                <div class="row">
                    <div class="col px-0">
                       <a href="{{ route('home') }}" class="text-decoration-none text-dark"><h1 class="py-4 text-center">Muñecos De Trapo Shop</h1></a> 
                    </div>
                </div>
            </header>

    <!--  -->
    <!-- <div class="container">
        
    </div> -->

    <div class="container pb-4">
            @if(count($cart))
        <div class="card shopping-cart">
                 <div class="card-header bg-dark  text-light">
                     <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                     Carrito de la compra
                     <a href="{{ route('home') }}" class="btn btn-outline-info btn-sm pull-right">Seguir comprando</a>
                     <a href="{{ route('cart-trash') }}" class="btn btn-outline-info btn-sm pull-right">Vaciar carrito</a>
                     <!-- <div class="clearfix"></div> -->
                 </div>
                 <div class="card-body pb-4">
                    
                         <!-- PRODUCT -->
                         @foreach($cart as $product)
                         <div class="row">
                             <div class="col-12 col-sm-12 col-md-2 text-center">
                                     <img class="img-responsive" src="{{ $product->ruta }}" alt="prewiew" width="140" height="140">
                             </div>
                             <div class="col-12 text-sm-center col-sm-12 text-md-left col-md-6">
                                 <h4 class="product-name"><strong>{{ $product->name }}</strong></h4>
                                 <h4>
                                     <small>{{ $product->description }}</small>
                                 </h4>
                             </div>
                             <div class="col-12 col-sm-12 text-sm-center col-md-4 text-md-right row">
                                 <div class="col-3 col-sm-3 col-md-6 text-md-right" style="padding-top: 5px">
                                     <h6><strong>{{ number_format($product->price,2) }}€<span class="text-muted"> x</span></strong></h6>
                                 </div>
                                 <div class="col-4 col-sm-4 col-md-4">
                                     <div class="quantity">
                                         <input type="number" step="1" max="99" min="1" value="{{ $product->quantity }}"  title="Qty" class="qty"
                                                size="4" id="product_{{ $product->id }}">
                                     </div>
                                 </div>
                                 <div class="col-2 col-sm-2 col-md-2 text-right">
                                <a href="{{ route('cart-update', $product->id)}}" class="btn btn-outline-secondary btn-xs text-decoration-none" data-href="{{ route('cart-update', $product->id) }}"
										data-id = "{{ $product->id }}">
                                         <i class="fa fa-refresh" aria-hidden="true"></i></a> 
                                <a href="{{ route('cart-delete', $product->id ) }}"><button type="button" class="btn btn-outline-danger btn-xs mt-2 text-decoration-none">
                                                <i class="fa fa-trash" aria-hidden="true"></i></a>
                                     </button>
                                 </div>
                             </div>
                         </div>
                         <hr>
                         @endforeach 
                 </div>
                 <div class="card-footer">
                     <div class="pull-right" style="margin: 10px">
                            <div class="pull-right pb-2" style="margin: 5px">
                                    Total : {{ number_format($total,2) }} €
                                </div>
                         <a href="{{ route('order-detail') }}" class="btn btn-info pull-right">Continuar</a>
                         
                     </div>
                 </div>
                 
             </div>
             @else
             <h3><span class="label label-warning">No hay productos en el carrito</span></h3>
             @endif
     </div>
     
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
     <script src="{{asset('js/main.js')}}"></script>
     

</body>
</html>