<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Reenie+Beanie&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/0763b1b783.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/stylepago.css') }}">
    <title>Política</title>
</head>
<body>
    <header class="d-block">
        <div class="row">
            <div class="col px-0"> 
               <a href="{{ route('home') }}" class="text-decoration-none text-dark"><h1 class="py-4 text-center">Muñecos De Trapo Shop</h1></a> 
            </div>
        </div>
    </header>

    <!--  -->
    <div class="container">
            <div class="col text-left">
                    <a href="#" class="text-decoration-none"><i class="far fa-handshake"></i><span> Política</span></a>
                </div> 
            <ul class="list-group list-group-flush pt-4">
                <li class="list-group-item"><b>QUIÉNES SOMOS?</b> Nosotros somos MuñecosDeTrapoShop y tratamos tus datos personales como corresponsables
del tratamiento. Esto quiere decir que nos hacemos cargo conjuntamente de cómo usar y
proteger tus datos.</li>
                <li class="list-group-item"><b>¿PARA QUÉ USAMOS TUS DATOS?</b> Usaremos tus datos (obtenidos
online o en persona), entre otras finalidades, para gestionar tu registro como usuario,
gestionar la compra de productos o servicios, atender tus consultas así como para, en
caso de que lo desees, enviarte nuestras comunicaciones personalizadas.</li>
                <li class="list-group-item"><b>¿POR QUÉ LOS USAMOS?</b> Estamos legitimados para tratar tus datos por
diferentes motivos. El principal, es que necesitamos tratarlos para ejecutar el contrato que
aceptas con nosotros al registrarte y al hacer una compra o disfrutar de alguno de nuestros
servicios o funcionalidades, aunque hay otras razones que nos legitiman a ello, como el
interés en atender tus consultas o el consentimiento que nos prestas para enviarte
nuestras newsletter, entre otras</li>
                <li class="list-group-item"><b>¿CON QUIÉN COMPARTIMOS TUS DATOS?</b>Compartiremos tus
datos con prestadores de servicios que nos ayudan o dan soporte, ya sean empresas del
propio Grupo Inditex o colaboradores externos con quien hemos llegado a un acuerdo, y
ya estén ubicados dentro o fuera de la Unión Europea.</li>
                <li class="list-group-item"><b>TUS DERECHOS.</b>Tienes derecho a acceder, rectificar o suprimir tus datos
personales. En algunos casos, también tienes otros derechos, por ejemplo, a oponerte a
que usemos tus datos o a portarlos,</li>
            </ul>
    </div>
</body>
</html>