<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Reenie+Beanie&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/0763b1b783.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/styleitem.css') }}">
    <title></title>
</head>
<body class="overflow-x-hidden">
        <header class="d-block">
                <div class="row">
                    <div class="col px-0">
                       <a href="{{ route('home') }}" class="text-decoration-none text-dark"><h1 class="py-4 text-center">Muñecos De Trapo Shop</h1></a> 
                    </div>
                </div>
            </header>

        {{-- <div class="row container pb-4">
            <div class="col text-center">
                <a href=""><i class="fas fa-shopping-cart fa-2x"></i><span> Detalle del producto</span></a>
            </div> 
        </div> --}}
    </header>

    <div class="container text-center pb-4">
        <section class="main row ">
            <article class="col-xs12 col-sm-12 col-md-8 col-lg-8">
                @if(!is_null($products->ruta) and !empty($products->ruta))
                    <img src="{{strtolower(substr($_SERVER['SERVER_PROTOCOL'],0,5))=='https://'?'https://':'http://'}}{{$_SERVER['HTTP_HOST']}}{{$products->ruta}}" width="550" height="550" class="img-fluid"></img>
                @endif
                    <!-- <img src="img/1.jpg" class="img-fluid"> -->
            </article>
            <aside class="col-xs12 col-sm-12 col-md-4 col-lg-4">
                    <div class="row container">
                            <div class="card container">
                                    <div class="card-body">
                                      <h5 class="card-title">{{ $products->name }}</h5>
                                      <h6 class="card-subtitle mb-2 text-muted">{{ $category->name }}</h6>
                                      <p class="card-text">{{ $products->description }}</p>
                                      <p class="card-text">Precio: {{ number_format($products->price,2) }}€</p>
                                      <p>
                                          <a class=" btn btn-light btn-block" href="{{ route('cart-add' , $products->id) }}">
                                            Comprar <i class="fa fa-cart-plus fa-2x"></i>
                                          </a>
                                      </p>
                                      
                                    </div>
                            </div>
                    </div>
            </aside>
        </section>   
      </div>
      <footer class="bg-dark full-width pt-4">
            <div class="container py-4">
                <div class="py-4 row text-white">
                    <div class="col">
                        <h4>EMPRESA</h4>
                        <a class="d-block text-decoration-none" href="{{ route('conditions') }}" ><i class="fas fa-shopping-bag "></i> <span>CONDICIONES DE USO Y COMPRA</span></a> 
                    </div>
    
                    <div class="col">
                        <h4>SIGUENOS</h4>
                        <a class="d-block text-decoration-none" href="https://www.instagram.com/munecosdetraposhop/?hl=es"><i class="fab fa-instagram"></i><span> INSTAGRAM</span></a>
                    </div>
    
                    <div class="col">
                        <h4>AYUDA</h4>
                        <a class="d-block text-decoration-none" href="{{ route('pago') }}"><i class="fab fa-cc-amazon-pay"></i><span> PAGO</span></a>
                        <a class="d-block text-decoration-none" href="{{ route('envio') }}"><i class="far fa-paper-plane"></i><span> ENVÍO</span></a>
                        <a class="d-block text-decoration-none" href="{{ route('politics') }}"><i class="fab fa-product-hunt"></i><span> POLÍTICA</span></a>  
                    </div>                   
                </div>
            </div>
        </footer>


    
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>