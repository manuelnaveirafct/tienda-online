@extends('config.product.default')

@section('container')
    <h5> {{ $product->name }}</h5>
    <p>Categoria: {{ $product->category->name }}</p>
    <p>Description: {{ $product->description }}</p>
    <p>Precio: {{ $product->price }}</p>
    <p>Stock: {{ $product->stock }}</p>
    <p>Discount {{ $product->discount ?? 'No hay descuento' }}</p>

    <div class="row">
        @foreach($product->images as $image)
            <div class="col-2 lodgingImg">
                ​<figure class="figure">
                    <form action="{{ route('config.product.image.delete', [$product, $image]) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">
                            <i class="las la-trash"></i>
                        </button>
                    </form>
                    <img id="{{ $image->id }}" draggable="true" src="{{ route('config.product.image.show', [$product, $image]) }}" class="draggable-image rounded float-left img-thumbnail figure-img img-fluid"  alt="">
                    <figcaption class="figure-caption">{{ $image->name }}</figcaption>
                </figure>
            </div>
        @endforeach
    </div>
    
    <a href="{{ route('config.product.edit', $product) }}">Editar</a>

    <form action="{{ route('config.product.image.store', $product) }}" class="dropzone">
        <div class="fallback">
          <input name="file" type="file" multiple />
        </div>
    </form>

    <form action="{{ route('config.product.delete', $product) }}" method="POST">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger" type="submit">Eliminar</button>
    </form>
@endsection