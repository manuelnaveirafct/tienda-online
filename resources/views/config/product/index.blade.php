@extends('config.product.default')

@section('container')

<div class="row">

    @foreach ($products as $product)

      <div class="col-sm-4 my-2">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">{{ $product->name }}</h5>
            <p class="card-text">{{ $product->description }}</p>
            <p class="container text-center">
            @if(!is_null($product->ruta) and !empty($product->ruta))
              <img src="{{strtolower(substr($_SERVER['SERVER_PROTOCOL'],0,5))=='https://'?'https://':'http://'}}{{$_SERVER['HTTP_HOST']}}{{$product->ruta}}" width="150" height="150"></img>
            @endif
            </p>
            <a href="{{ route('config.product.show', $product) }}" class="btn btn-block btn-primary">Ver producto</a>
          </div>
        </div>
        
      </div>

    @endforeach

  </div>

  <div class="row mt-4">
    <div class="col text-center">
      <a href="{{ route('config.product.create') }}" class="btn btn-lg btn-success">Crear producto</a>
    </div>
  </div>


@endsection