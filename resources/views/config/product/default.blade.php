@extends('app')

@section('content')
    <div class="border-0 jumbotron py-4">
        <h1 class="display-4"><a href="{{ route('config.product.index') }}">Productos</a></h1>
        <p class="lead">Panel de configuración de productos</p>
      </div>

    @yield('container')

@endsection

