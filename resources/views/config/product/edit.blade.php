@extends('config.product.default')
<link rel="stylesheet" href="{{ asset('css/styleedit.css') }}">

@section('container')

    <form action="{{ route('config.product.update', $product) }}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="content">Nombre</label>
            <input type="text" name="name" value="{{ old('name') ?? $product->name }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
            @if ($errors->has('name'))
                <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <select class="form-control {{ $errors->has('category_id') ? 'is-invalid' : ''}}" name="category_id" value="{{ old('category_id') ?? $product->category_id }}" id="category">
              @foreach($categories as $category)
                <option {{ $category->id == $product->category_id ? 'selected' : null }} value="{{$category->id}}">{{$category->name}}</option>
              @endforeach
            </select>
            @if ($errors->has('category_id'))
                <div class="invalid-feedback">
                    {{ $errors->first('category_id') }}
                </div>
            @endif
          </div>

        <div class="form-group">
            <label for="description">Descripcion</label>
            <textarea name="description" id="category" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" rows="5">{{ old('description') ?? $product->description }}</textarea>
            @if ($errors->has('description'))
                <div class="invalid-feedback">
                    {{ $errors->first('description') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Precio</label>
            <input type="text" name="price" value="{{ old('price') ?? $product->price }}" class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}">
            @if ($errors->has('price'))
                <div class="invalid-feedback">
                    {{ $errors->first('price') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Stock</label>
            <input type="text" name="stock" value="{{ old('stock') ?? $product->stock }}" class="form-control {{ $errors->has('stock') ? 'is-invalid' : '' }}">
            @if ($errors->has('stock'))
                <div class="invalid-feedback">
                    {{ $errors->first('stock') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Descuento</label>
            <input type="text" name="discount" value="{{ old('discount') ?? $product->discount }}" class="form-control {{ $errors->has('discount') ? 'is-invalid' : '' }}">
            @if ($errors->has('discount'))
                <div class="invalid-feedback">
                    {{ $errors->first('discount') }}
                </div>
            @endif
        </div>

        <button class="btn btn-primary" type="submit">Guardar</button>

    </form>
@endsection