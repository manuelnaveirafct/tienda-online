@extends('config.product.default')

@section('container')
    <form action="{{ route('config.product.store') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="content">Nombre</label>
            <input type="text" name="name" value="{{ old('name') ?? null }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
            @if ($errors->has('name'))
                <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <select class="form-control {{ $errors->has('category_id') ? 'is-invalid' : '' }}" name="category_id">
              @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
              @endforeach
            </select>
            @if ($errors->has('category_id'))
                <div class="invalid-feedback">
                    {{ $errors->first('category_id') }}
                </div>
            @endif
          </div>

        <div class="form-group">
            <label for="content">Descripción</label>
            <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" rows="5" name="description">{{ old('description') ?? null }}</textarea>
            @if ($errors->has('description'))
                <div class="invalid-feedback">
                    {{ $errors->first('description') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Precio</label>
            <input type="text" name="price" value="{{ old('price') ?? null }}" class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}">
            @if ($errors->has('price'))
                <div class="invalid-feedback">
                    {{ $errors->first('price') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Stock</label>
            <input type="text" name="stock" value="{{ old('stock') ?? null }}" class="form-control {{ $errors->has('stock') ? 'is-invalid' : '' }}">
            @if ($errors->has('stock'))
                <div class="invalid-feedback">
                    {{ $errors->first('stock') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Descuento</label>
            <input type="text" name="discount" value="{{ old('discount') ?? null }}" class="form-control {{ $errors->has('discount') ? 'is-invalid' : '' }}">
            @if ($errors->has('discount'))
                <div class="invalid-feedback">
                    {{ $errors->first('discount') }}
                </div>
            @endif
        </div>

        <button class="btn btn-primary" type="submit">Guardar</button>

    </form>
@endsection