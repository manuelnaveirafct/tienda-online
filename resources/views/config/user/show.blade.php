@extends('config.user.default')

@section('container')
    <p>Nombre: {{ $user->name }}</p>
    {{-- <p>Categoria: {{ $product->category->name }}</p> --}}
    <p>Apellido: {{ $user->surname }}</p>
    <p>Dirección: {{ $user->address }}</p>
    <p>Localidad: {{ $user->localidad }}</p>
    <p>Provincia {{ $user->provincia }}</p>
    <p>Email {{ $user->email }}</p>

    
    <a href="{{ route('config.user.edit', $user) }}">Editar</a>

    <form action="{{ route('config.user.delete', $user) }}" method="POST">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger" type="submit">Eliminar</button>
    </form>
@endsection