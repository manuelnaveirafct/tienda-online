@extends('config.user.default')

@section('container')

<div class="row">
    @foreach ($users as $user)

    <div class="col-md-4 my-2">
      <div class="card">
        <div class="card body">
          <h4><i class="fas fa-users fa-2x"></i></h4>
          <h5 class="card-title text-center">{{ ucwords($user->name) }} {{ ucwords($user->surname) }} </h5>
          <p> <a href="{{ route('config.user.show', $user) }}" class="btn btn-block btn-primary">Ver usuario</a> </p>
      </div>
    </div>
</div>
      <!-- <p> <a href="{{ route('config.user.show', $user) }}">{{ $user->name }}</a> </p> -->
    @endforeach
</div>

    <a href="{{ route('config.user.create') }}" class="btn btn-success">Crear Usuario</a>
@endsection

