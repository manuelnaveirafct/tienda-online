@extends('config.user.default')

@section('container')

    <form action="{{ route('config.user.update', $user) }}" method="post">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="content">Email</label>
            <input type="text" name="email" value="{{ old('email') ?? $user->email }}" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}">
            @if ($errors->has('email'))
                <div class="invalid-feedback">
                    {{ $errors->first('email') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Contraseña</label>
            <input type="password" name="password" value="{{ old('password') ?? $user->password }}" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}">
            @if ($errors->has('password'))
                <div class="invalid-feedback">
                    {{ $errors->first('password') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Nombre</label>
            <input type="text" name="name" value="{{ old('name') ?? $user->name }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
            @if ($errors->has('name'))
                <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Apellido</label>
            <textarea name="surname" class="form-control {{ $errors->has('surname') ? 'is-invalid' : '' }}" rows="5">{{ old('surname') ?? $user->surname }}</textarea>
            @if ($errors->has('surname'))
                <div class="invalid-feedback">
                    {{ $errors->first('surname') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Dirección</label>
            <input type="text" name="address" value="{{ old('address') ?? $user->address }}" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}">
            @if ($errors->has('address'))
                <div class="invalid-feedback">
                    {{ $errors->first('address') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Localidad</label>
            <input type="text" name="localidad" value="{{ old('localidad') ?? $user->localidad }}" class="form-control {{ $errors->has('localidad') ? 'is-invalid' : '' }}">
            @if ($errors->has('localidad'))
                <div class="invalid-feedback">
                    {{ $errors->first('localidad') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Provincia</label>
            <input type="text" name="provincia" value="{{ old('provincia') ?? $user->provincia }}" class="form-control {{ $errors->has('provincia') ? 'is-invalid' : '' }}">
            @if ($errors->has('provincia'))
                <div class="invalid-feedback">
                    {{ $errors->first('provincia') }}
                </div>
            @endif
        </div>

        <button class="btn btn-primary" type="submit">Guardar</button>

    </form>
@endsection