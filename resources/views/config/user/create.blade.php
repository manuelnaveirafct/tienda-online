@extends('config.user.default')

@section('container')
    <form action="{{ route('config.user.store') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="content">Email</label>
            <input type="text" name="email" value="{{ old('email') ?? null }}" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}">
            @if ($errors->has('email'))
                <div class="invalid-feedback">
                    {{ $errors->first('email') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Contraseña</label>
            <input type="password" name="password" value="{{ old('password') ?? null }}" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}">
            @if ($errors->has('password'))
                <div class="invalid-feedback">
                    {{ $errors->first('password') }}
                </div>
            @endif
        </div>
        
        <div class="form-group">
            <label for="content">Nombre</label>
            <input type="text" name="name" value="{{ old('name') ?? null }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
            @if ($errors->has('name'))
                <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Apellido</label>
            <input type="text" name="surname" value="{{ old('surname') ?? null }}" class="form-control {{ $errors->has('surname') ? 'is-invalid' : '' }}">
            @if ($errors->has('surname'))
                <div class="invalid-feedback">
                    {{ $errors->first('surname') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Dirección</label>
            <input type="text" name="address" value="{{ old('address') ?? null }}" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}">
            @if ($errors->has('address'))
                <div class="invalid-feedback">
                    {{ $errors->first('address') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Localidad</label>
            <input type="text" name="localidad" value="{{ old('localidad') ?? null }}" class="form-control {{ $errors->has('localidad') ? 'is-invalid' : '' }}">
            @if ($errors->has('localidad'))
                <div class="invalid-feedback">
                    {{ $errors->first('localidad') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Provincia</label>
            <input type="text" name="provincia" value="{{ old('provincia') ?? null }}" class="form-control {{ $errors->has('provincia') ? 'is-invalid' : '' }}">
            @if ($errors->has('provincia'))
                <div class="invalid-feedback">
                    {{ $errors->first('provincia') }}
                </div>
            @endif
        </div>

        <button class="btn btn-primary" type="submit">Guardar</button>

    </form>
@endsection