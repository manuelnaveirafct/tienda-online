@extends('app')
@section('content')
    <div class="jumbotron py-2">
        <h1 class="display-4"><a href="{{ route('config.user.index') }}">Usuarios</a></h1>
        <p class="lead">Panel de configuración de usuarios</p>
      </div>
    @yield('container')
@endsection

