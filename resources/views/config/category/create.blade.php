@extends('config.category.default')

@section('container')
    <form action="{{ route('config.category.store') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="content">Nombre</label>
            <input type="text" name="name" value="{{ old('name') ?? null }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
            @if ($errors->has('name'))
                <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                </div>
            @endif
        </div>

        <button class="btn btn-primary" type="submit">Guardar</button>

    </form>
@endsection