@extends('config.category.default')

@section('container')
  <div class="row">

    @foreach ($categories as $category)

      <div class="col-sm-4 my-2">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">{{ $category->name }}</h5>
            <p class="card-text">{{ $category->name }}</p>
            <a href="{{ route('config.category.show', $category) }}" class="btn btn-block btn-primary">Ver categoría</a>
          </div>
        </div>
      </div>

    @endforeach

  </div>

  <div class="row mt-4">
    <div class="col text-center">
      <a href="{{ route('config.category.create') }}" class="btn btn-lg btn-success">Crear categoría</a>
    </div>
  </div>

    
@endsection