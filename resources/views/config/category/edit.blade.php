@extends('config.category.default')
<link rel="stylesheet" href="{{ asset('css/styleedit.css') }}">

@section('container')
    <form action="{{ route('config.category.update', $category) }}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="content">Nombre</label>
            <input type="text" name="name" value="{{ old('name') ?? $category->name }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
            @if ($errors->has('name'))
                <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                </div>
            @endif
        </div>

        <button class="btn btn-primary" type="submit">Guardar</button>

    </form>
@endsection