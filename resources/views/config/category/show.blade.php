@extends('config.category.default')

@section('container')
    <h5>{{ $category->name }}</h5>
    
    <a href="{{ route('config.category.edit', $category) }}">Editar</a>

    <form action="{{ route('config.category.delete', $category) }}" method="POST">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger" type="submit">Eliminar</button>
    </form>
@endsection