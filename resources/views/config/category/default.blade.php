@extends('app')

@section('content')
    <div class="border-0 jumbotron py-4">
        <h1 class="display-4"><a href="{{ route('config.category.index') }}">Categorías</a></h1>
        <p class="lead">Panel de configuración de categorías</p>
      </div>
    @yield('container')
@endsection

