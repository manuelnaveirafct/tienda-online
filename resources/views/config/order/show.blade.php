@extends('config.order.default')

@section('container')
    <h5>Total: {{ $order->subtotal }}€</h5>
    <h5>Gastos de envío: {{ $order->shipping }}€</h5>
    <h5>Id de usuario: {{ $order->user_id }}</h5>
    
    <a href="{{ route('config.order.edit', $order) }}">Editar</a>

    <form action="{{ route('config.order.delete', $order) }}" method="POST">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger" type="submit">Eliminar</button>
    </form>
@endsection