@extends('config.order.default')
<link rel="stylesheet" href="{{ asset('css/styleedit.css') }}">

@section('container')
    <form action="{{ route('config.order.update', $order) }}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="content">Total</label>
            <input type="text" name="subtotal" value="{{ old('subtotal') ?? $order->subtotal }}" class="form-control {{ $errors->has('subtotal') ? 'is-invalid' : '' }}">
            @if ($errors->has('subtotal'))
                <div class="invalid-feedback">
                    {{ $errors->first('subtotal') }}
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="content">Envío</label>
            <input type="text" name="shipping" value="{{ old('shipping') ?? $order->shipping }}" class="form-control {{ $errors->has('shipping') ? 'is-invalid' : '' }}">
            @if ($errors->has('shipping'))
                <div class="invalid-feedback">
                    {{ $errors->first('shipping') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Id de usuario</label>
            <input type="text" name="user_id" value="{{ old('user_id') ?? $order->user_id }}" class="form-control {{ $errors->has('user_id') ? 'is-invalid' : '' }}">
            @if ($errors->has('user_id'))
                <div class="invalid-feedback">
                    {{ $errors->first('user_id') }}
                </div>
            @endif
        </div>

        <button class="btn btn-primary" type="submit">Guardar</button>

    </form>
@endsection