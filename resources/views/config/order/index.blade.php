@extends('config.order.default')

@section('container')
  <div class="row">
    
    @foreach ($orders as $order)
      <div class="col-sm-4 my-2">
        <div class="card">
          <div class="card-body text-center">
          <i class="fas fa-box-open fa-2x"></i>
            @foreach ($users as $user)
            @if ( $order->user_id == $user->id)
            <h4>{{ ucwords($user->name) }} {{ ucwords($user->surname) }}</h4>
            {{ $order->created_at->diffForHumans() }}
            @endif
            
            @endforeach
            <a href="{{ route('config.order.show', $order) }}" class="btn btn-block btn-primary">Ver pedido</a>
          </div>
        </div>
      </div>

    @endforeach

  </div>

  <div class="row mt-4">
    <div class="col text-center">
      <a href="{{ route('config.order.create') }}" class="btn btn-lg btn-success">Crear pedido</a>
    </div>
  </div>

    
@endsection