@extends('config.order.default')

@section('container')
    <form action="{{ route('config.order.store') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="content">Total</label>
            <input type="text" name="subtotal" value="{{ old('subtotal') ?? null }}" class="form-control {{ $errors->has('subtotal') ? 'is-invalid' : '' }}">
            @if ($errors->has('subtotal'))
                <div class="invalid-feedback">
                    {{ $errors->first('subtotal') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Gastos de envío</label>
            <input type="text" name="shipping" value="{{ old('shipping') ?? null }}" class="form-control {{ $errors->has('shipping') ? 'is-invalid' : '' }}">
            @if ($errors->has('shipping'))
                <div class="invalid-feedback">
                    {{ $errors->first('shipping') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="content">Id de usuario</label>
            <input type="text" name="user_id" value="{{ old('user_id') ?? null }}" class="form-control {{ $errors->has('user_id') ? 'is-invalid' : '' }}">
            @if ($errors->has('user_id'))
                <div class="invalid-feedback">
                    {{ $errors->first('user_id') }}
                </div>
            @endif
        </div>

        <button class="btn btn-primary" type="submit">Guardar</button>

    </form>
@endsection