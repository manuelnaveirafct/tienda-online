<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Reenie+Beanie&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/0763b1b783.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/stylepago.css') }}">
    <title>Envío</title>
</head>
<body>
    <header class="d-block">
        <div class="row">
            <div class="col px-0">
               <a href="{{ route('home') }}" class="text-decoration-none text-dark"><h1 class="py-4 text-center">Muñecos De Trapo Shop</h1></a> 
            </div>
        </div>
    </header>

    <!--  -->
    <div class="container">
            <div class="col text-left">
                    <a href="#" class="text-decoration-none"><i class="fas fa-box-open 2-px"></i><span > Envío</span></a>
                </div> 
            <ul class="list-group list-group-flush pt-4">
                <li class="list-group-item"><b>Las opciones de envío pueden variar en función de la dirección de entrega, la hora de tu compra y la disponibilidad de los artículos.</b></li>
                <li class="list-group-item"><b>Recogida en Narón- GRATUITO</b></li>
                <li class="list-group-item"><b>Entrega a domicilio: Al día siguiente de la compra - 3,95 EUR / GRATUITO (PEDIDOS A PARTIR DE 30 EUR)</b></li>
                <li class="list-group-item">Nuestro objetivo es entregar al día siguiente todos los pedidos realizados de lunes a viernes antes de las 21h. Cuando esto no sea posible, te mostraremos el plazo estimado de entrega en el momento de la compr</li>
                <li class="list-group-item"><b>En el mismo día de la compra - 5,95 EUR</b></li>
                <li class="list-group-item">Entrega en el mismo día  para todos los pedidos realizados de lunes a viernes antes de las 14h</li>
            </ul>
    </div>
</body>
</html>