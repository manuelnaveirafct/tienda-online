<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Reenie+Beanie&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/0763b1b783.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/stylepago.css') }}">
    <title>Condiciones de compra</title>
</head>
<body>
        <header class="d-block">
                <div class="row">
                    <div class="col px-0">
                       <a href="{{ route('home') }}" class="text-decoration-none text-dark"><h1 class="py-4 text-center">Muñecos De Trapo Shop</h1></a> 
                    </div>
                </div>
            </header>

    <!--  -->
    <div class="container">
            <div class="col text-left">
                    <a class="text-decoration-none" href="#"><i class="fas fa-cubes"></i><span> Condiciones de uso y compra</span></a>
                </div> 
            <ul class="list-group list-group-flush pt-4">
                <li class="list-group-item"><b>INTRODUCCIÓN <hr></b>El presente documento  establece las
                    condiciones por las que se rige el uso de esta página web  y la compra de productos en
                    la misma , cualquiera que sea el aplicativo, medio digital, soporte o
                    dispositivo a través del que se pueda acceder.
                </li>

                <li class="list-group-item"><b>SUS DATOS Y SUS VISITAS A ESTA PÁGINA WEB <hr></b>La información o datos personales que nos facilite sobre usted serán tratados con arreglo a lo
                    establecido en la Política de Privacidad y Cookies. Al hacer uso de esta página web usted consiente el
                    tratamiento de dicha información y datos y declara que toda la información o datos que nos facilite son
                    veraces y se corresponden con la realidad.
                </li>

                <li class="list-group-item"><b>USO DE NUESTRO SITIO WEB <hr></b>Al hacer uso de esta página web y realizar pedidos a través de la misma usted se compromete a: <br>
                    i. Hacer uso de esta página web únicamente para realizar consultas o pedidos legalmente
                    válidos. <br>
                    ii. No realizar ningún pedido falso o fraudulento. Si razonablemente se pudiera considerar que
                    se ha hecho un pedido de esta índole estaremos autorizados a anularlo e informar a las
                    autoridades pertinentes. <br>
                    iii. Facilitarnos su dirección de correo electrónico, dirección postal y/u otros datos de contacto
                    de forma veraz y exacta. Asimismo, consiente que podremos hacer uso de dicha información
                    para ponernos en contacto con usted si es necesario .
                    Si no nos facilita usted toda la información que necesitamos, no podremos cursar su pedido.
                    Al realizar un pedido a través de esta página web, usted declara ser mayor de 18 años y tener
                    capacidad legal para celebrar contratos.

                </li>

                <li class="list-group-item"><b>DISPONIBILIDAD DEL SERVICIO <hr></b>Los artículos que se ofrecen a través de esta página web están únicamente disponibles para su envío a
                    territorio español.
                </li>

                <li class="list-group-item"><b>CÓMO REALIZAR UN PEDIDO<hr></b>Para realizar un pedido, deberá seguir el procedimiento de compra online
                </li>

                <li class="list-group-item"><b>DISPONIBILIDAD DE LOS PRODUCTOS<hr></b>Todos los pedidos están sujetos a la disponibilidad de los productos. Si se produjeran dificultades en
                    cuanto al suministro de productos o si no quedan artículos en stock, le reembolsaremos cualquier
                    cantidad que pudiera usted haber abonado.
                </li>

                <li class="list-group-item"><b>PRECIO Y PAGO<hr></b>Los precios de la página web incluyen IVA.Los precios pueden cambiar en cualquier momento, pero (salvo en lo establecido anteriormente) los
                    posibles cambios no afectarán a los pedidos con respecto a los que ya le hayamos enviado una
                    Confirmación de Pedido.
                    Una vez que haya seleccionado todos los artículos que desea comprar, estos se habrán añadido a su cesta
                    y el paso siguiente será tramitar el pedido y efectuar el pago. Para ello, deberá seguir los pasos del proceso
                    de compra, rellenando o comprobando la información que en cada paso se le solicita. 
                </li>

                <li class="list-group-item"><b>IMPUESTO SOBRE EL VALOR AÑADIDO Y FACTURACIÓN<hr></b>De conformidad con lo dispuesto en el artículo 68 de la Ley 37/1992, de 28 de diciembre, del Impuesto 
                    sobre el Valor Añadido, la entrega de los artículos se entenderá localizada en el territorio de aplicación
                    del IVA español si la dirección de entrega está en territorio español salvo Canarias, Ceuta y Melilla. El tipo
                    de IVA aplicable será el legalmente vigente en cada momento en función del artículo concreto de que se
                    trate.

                </li>

                <li class="list-group-item"><b>POLÍTICA DE DEVOLUCIONES<hr></b>Si usted está contratando como consumidor y usuario, tiene usted derecho a desistir del presente
                    contrato en un plazo de 14 días naturales sin necesidad de justificación.
                    El plazo de desistimiento expirará a los 14 días naturales del día que usted o un tercero por usted indicado,
                    distinto del transportista, adquirió la posesión material de los bienes o en caso de que los bienes que
                    componen su pedido se entreguen por separado, a los 14 días naturales del día que usted o un tercero
                    por usted indicado, distinto del transportista, adquirió la posesión material del último de esos bienes.


                </li>
            </ul>
       
    </div>
</body>
</html>