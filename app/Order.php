<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'orders';

    protected $fillable = ['subtotal', 'shipping', 'user_id'];

    public function user()
	{
		return $this->belongsTo('App\Models\User');
    }
    
	public function orderDetail()
	{
		return $this->hasMany('App\OrderItem');
	}
}
