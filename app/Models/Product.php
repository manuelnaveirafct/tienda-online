<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Imageable;

class Product extends Model
{
    use Imageable;

    protected $fillable = [
        'category_id',
        'name',
        'description',
        'price',
        'stock',
        'discount',
        'ruta'
    ];

     /**
     * ELOQUENT RELATIONSHIPS
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
