<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PanelController extends Controller
{
    public function index()
    {
        $this->authorize('handle', request()->user());
        return view('confpanel');
    }
}
