<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use App\Models\Product;

class SearchController extends Controller
{

    public function search(Request $request)
    {
        $search = $request['search'];
        
        $products = Product::where('name', 'like', "%{$search}%")->get();

        return ProductResource::collection($products);
    }
   
}
