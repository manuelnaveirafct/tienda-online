<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class CartController extends Controller
{
    //creamos variable de sesión
    public function __construct()
    {
        if(!\Session::has('cart')) \Session::put('cart',array());
    }

    //show  cart
    public function show()
    {
        $cart = \Session::get('cart');
        $total = $this->total();

        return view('cart',compact('cart','total'));
    }
    //add item
    public function add(Product $product)
    {
        $cart = \Session::get('cart');
        $product->quantity = 1;
        $cart[$product->id] = $product;
        \Session::put('cart', $cart);

        return redirect()->route('cart-show');
    }
    //delete item
    public function delete(Product $product)
    {
        $cart = \Session::get('cart');
        unset($cart[$product->id]);
        \Session::put('cart', $cart);

        return redirect()->route('cart-show');

    }
    //update item
    public function update(Product $product, $quantity)
    {
        $cart = \Session::get('cart');
        $cart[$product->id]->quantity = $quantity;
        \Session::put('cart',$cart);

        return redirect()->route('cart-show');

    }

    // trash cart
   public function trash()
   {
        \Session::forget('cart');
        return redirect()->route('cart-show');
   }

    //total
    private function total()
    {
        $cart = \Session::get('cart');
        $total = 0;
        foreach($cart as $product){
            $total += $product->price * $product->quantity;
        }

        return $total;
    }

    //detalle del producto

    public function orderDetail()
    {
        if(count(\Session::get('cart')) <= 0) return redirect()->route('home');
        $cart = \Session::get('cart');
        $total = $this->total();

        return view('order-detail', compact('cart','total'));
        

    }
}