<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::all();

        return view('home', compact('categories'));
    }

    public function login()
    {
        return view('logueado');
    }

    public function pago()
    {
        return view('pago');
    }

    public function envio()
    {
        return view('envio');
    }

    public function politics()
    {
        return view('politics');
    }

    public function conditions()
    {
        return view('conditions');
    }
   
}
