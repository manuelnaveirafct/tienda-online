<?php

namespace App\Http\Controllers\Config\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CategoryRequest;

use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('handle', request()->user());

        $categories = Category::all();

        return view('config.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('handle', request()->user());
        return view('config.category.create');
    }

    /**
     * Undocumented function
     *
     * @param CategoryStoreRequest $request
     * @return void
     */
    public function store(CategoryRequest $request)
    {
        $this->authorize('handle', request()->user());
        $category = Category::create([
            'name' => $request->name,
        ]);

        return redirect()->route('config.category.show', $category);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $this->authorize('handle', request()->user());
        return view('config.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $this->authorize('handle', request()->user());
        return view('config.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $this->authorize('handle', request()->user());
        $category->update([
            'name' => $request->name,
        ]);

        return redirect()->route('config.category.show', $category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function delete(Category $category)
    {
        $this->authorize('handle', request()->user());
        $category->delete();

        return redirect()->route('config.category.index');
    }
}
