<?php

namespace App\Http\Controllers\Config\Order;

use App\Http\Controllers\Controller;
use App\Http\Requests\Order\OrderRequest;

use App\Order;
use App\User;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('handle', request()->user());

        $orders = Order::all();
        $users = User::all();

        return view('config.order.index', compact('orders','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('handle', request()->user());
        return view('config.order.create');
    }

    /**
     * Undocumented function
     *
     * @param OrderStoreRequest $request
     * @return void
     */
    public function store(OrderRequest $request)
    {
        $this->authorize('handle', request()->user());

        $order = Order::create([
            'subtotal' => $request->subtotal,
            'shipping' => $request->shipping,
            'user_id' => $request->user_id,
        ]);

        return redirect()->route('config.order.show', $order);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $this->authorize('handle', request()->user());

        return view('config.order.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        $this->authorize('handle', request()->user());

        return view('config.order.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, Order $order)
    {
        $this->authorize('handle', request()->user());

        $order->update([
            'subtotal' => $request->subtotal,
            'shipping' => $request->shipping,
            'user_id' => $request->user_id,
        ]);

        return redirect()->route('config.order.show', $order);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $category
     * @return \Illuminate\Http\Response
     */
    public function delete(Order $order)
    {
        $this->authorize('handle', request()->user());

        $order->delete();

        return redirect()->route('config.order.index');
    }
}
