<?php

namespace App\Http\Controllers\Config\User;


use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use App\Http\Requests\User\UserRequest;
use App\User;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('handle', request()->user());

        $users = User::all();

        return view('config.user.index', compact('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $User
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $this->authorize('handle', request()->user());

        return view('config.user.create');
    }

    public function store(UserRequest $request)
    {
        $this->authorize('handle', request()->user());

        $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'name' => $request->name,
            'surname' => $request->surname,
            'address' => $request->address,
            'localidad' => $request->localidad,
            'provincia' => $request->provincia
        ]);

        return redirect()->route('config.user.show', $user);
    }

    public function show(User $user)
    {
        $this->authorize('handle', request()->user());

        return view('config.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $User
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('handle', request()->user());
        
        return view('config.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $User
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $this->authorize('handle', request()->user());

        $user->update([
            'name' => $request->name,
            'password' => Hash::make($request->password),
            'surname' => $request->surname,
            'address' => $request->address,
            'localidad' => $request->localidad,
            'provincia' => $request->provincia,
            'email' => $request->email
        ]);

        return redirect()->route('config.user.show', $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $User
     * @return \Illuminate\Http\Response
     */
    public function delete(User $user)
    {
        $this->authorize('handle', request()->user());
        
        $user->delete();

        return redirect()->route('config.user.index');
    }
}
