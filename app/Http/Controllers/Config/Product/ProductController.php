<?php

namespace App\Http\Controllers\Config\Product;

use App\Http\Controllers\Controller;

use App\Http\Requests\Product\ProductRequest;

use App\Models\Category;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('handle', request()->user());
        $products = Product::all();

        return view('config.product.index', compact('products'));
    }

    public function buscarPorCategoria($category)
    {
        
        $products = Product::where('category_id', $category)->get();

        return view('config.product.items', compact('products'));
    }

    public function findBy($productId)
    {
        $products = Product::find($productId);
        $category = Category::find($products->category_id);

        return view('config.product.item', compact('products', 'category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('handle', request()->user());
        $categories = Category::all();

        return view('config.product.create', compact('categories'));
    }

    /**
     * Undocumented function
     *
     * @param CategoryStoreRequest $request
     * @return void
     */
    public function store(ProductRequest $request)
    {
        $this->authorize('handle', request()->user());
        $product = Product::create([
            'name' => $request->name,
            'category_id'  => $request->category_id,
            'description'  => $request->description,
            'price'  => $request->price,
            'stock'  => $request->stock,
            'discount'  => $request->discount
        ]);

        return redirect()->route('config.product.show', $product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $this->authorize('handle', request()->user());
        return view('config.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $this->authorize('handle', request()->user());
        $categories = Category::all();
        return view('config.product.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $this->authorize('handle', request()->user());
        $product->update([
            'name' => $request->name,
            'category_id'  => $request->category_id,
            'description'  => $request->description,
            'price'  => $request->price,
            'stock'  => $request->stock,
            'discount'  => $request->discount
        ]);

        return redirect()->route('config.product.show', $product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function delete(Product $product)
    {
        $this->authorize('handle', request()->user());
        $product->delete();

        return redirect()->route('config.product.index');
    }
}
