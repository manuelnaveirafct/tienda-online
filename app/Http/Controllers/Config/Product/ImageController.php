<?php

namespace App\Http\Controllers\Config\Product;

use App\Models\Product;
use App\Http\Controllers\Controller;
use App\Http\Requests\Image\ImageRequest;
use App\Models\Resource\Image;

class ImageController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  string $image
     * @return \Illuminate\View\View
     */
    public function show(Product $product, Image $image)
    {
        return response()->file(storage_path("app/{$image->path}"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ImageRequest $request
     * @param product $product
     * @return void
     */
    public function store(ImageRequest $request, Product $product)
    {
        $image = $request->file('file')->store("products/{$product->id}");
        $product->images()->create(['id' => $product->id, 'path' => $image, 'product_id' => $product->id]);
        $product->update([
            'ruta' => "/config/product/{$product->id}/images/{$product->id}/show"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param product $product
     * @param Image $image
     * @return \Illuminate\Http\Response
     */
    public function delete(Product $product, Image $image)
    {
        $image->delete();

        return redirect()->route('config.product.show', $product);
    }
}
