<?php

namespace App\Traits;

use App\Models\Resource\Image;

trait Imageable
{
    /*
    |--------------------------------------------------------------------------
    | Boot
    |--------------------------------------------------------------------------
    */

    /**
     * On model deleting delete the related imageables
     *
     * @return void
     */
    protected static function bootImageable()
    {
        static::deleting(function ($model) {
            $model->images->each->delete();
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Eloquent Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * Get the model images.
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors & Mutators
    |--------------------------------------------------------------------------
    */

    /**
     * Get the images
     * @return array
     */
    public function getImagesAttribute()
    {
        return $this->images()->get();
    }

    /**
     * Get the hasImage attribute
     * @return bool
     */
    public function getHasImageAttribute()
    {
        return $this->images() != null;
    }
}
