<?php
return array(
    // set your paypal credential
    'client_id' => 'AWl3fOdLyLRF2k7nsf771b28ZivjrbjgOC2fd6Hvf5TTCF2Y2CzSbLMgr2Du3p5D6NWp25hKqlYOB40-',
    'secret' => 'EL1K9sI_1H50cq8oQaPwD5wVA9sb3OjUwSkr1YXKlrNYle7MDjMNh6bSOuOkqp1eFLEzr24GSSyc4mgv',
 
    /**
     * SDK configuration 
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',
 
        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,
 
        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,
 
        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',
 
        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);