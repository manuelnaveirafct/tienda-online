<?php

namespace Tests\Feature\Config\Category;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\User;

class CreateCategoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_view_create_form_category()
    {
        $user = factory(User::class)->create([
            'is_admin' => true
        ]);

        $this->actingAs($user);

        $response = $this->get(route('config.category.create'))
            ->assertStatus(200)
            ->assertSessionMissing('errors')
            ->assertViewIs('config.category.create');
    }

    /** @test */
    public function not_admin_users_cannot_view_create_form_category()
    {
        $user = factory(User::class)->create([
            'is_admin' => false
        ]);

        $this->actingAs($user);

        $response = $this->get(route('config.category.create'))
            ->assertStatus(403)
            ->assertSessionMissing('errors');
    }

    /** @test */
    public function guest_cant_view_create_form_category()
    {
        $response = $this->get(route('config.category.create'))
            ->assertStatus(302)
            ->assertSessionMissing('errors')
            ->assertRedirect(route('login'));
    }
}
