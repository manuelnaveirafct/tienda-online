<?php

namespace Tests\Feature\Category;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\User;
use App\Models\Category;

class StoreCategoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_store_category()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $response = $this->post(route('config.category.store'), [
            'name' => 'Patucos'
        ]);

        $this->assertDatabaseHas('categories', [
            'name' => 'Patucos'
        ]);

        $response->assertStatus(302)
            ->assertSessionMissing('errors')
            ->assertRedirect(route('config.category.show', Category::whereName('Patucos')->first()));
    }

    /** @test */
    public function guest_cant_store_category()
    {
        $response = $this->post(route('config.category.store'), []);

        $response->assertStatus(302)
            ->assertSessionMissing('errors')
            ->assertRedirect(route('login'));
    }

    /** @test */
    public function store_category_required_data()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $response = $this->from(route('config.category.create'))->post(route('config.category.store'), [])
            ->assertSessionHasErrors(['name']);

        $this->followRedirects($response)
            ->assertStatus(200)
            ->assertSee(trans('validation.required', ['attribute' => 'name']));
    }
}
